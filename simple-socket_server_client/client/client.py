#!/usr/bin/python

from socket import socket, AF_INET, SOCK_STREAM

is_init=False
host = '127.0.0.1'
port = 5555

def print_CLI():
    # print help menu in the beginning
    global is_init
    if( is_init == False ):
        print("If you want to exit this programme, input exit or ctrl+d")
        is_init=True

    print(">"*5+" ", end='')

def waitForInput():
    message = input()
    return message

def send_data_to_server(data):
    global host
    global port
    request = data
    s = socket(AF_INET, SOCK_STREAM)
    s.connect((host, port))
    s.send(str.encode(request))
    response = s.recv(1000)
    print(response)
    s.close()

if __name__ == "__main__":
    ret=""

    while( 1 ):
        print_CLI()

        try:
            ret = waitForInput()
            send_data_to_server(ret)
        except EOFError:
            break;

        if( ret == "exit" ):
            break

    print("Bye~~")


