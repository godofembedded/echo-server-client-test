import socket

port = 5555

def listen():
    global port

    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    connection.bind(('0.0.0.0', port))
    connection.listen(10)
    while True:
        current_connection, address = connection.accept()
        while True:
            data = current_connection.recv(2048)
            current_connection.send(data)

            current_connection.shutdown(1)
            current_connection.close()

            # For debug
            output = data.decode("utf-8")

            try:
                print(output)
            except:
                pass

            break

if __name__ == "__main__":
    print("Welcome to simple echo server. it will listen the port {0}".format(port))

    try:
        listen()
    except KeyboardInterrupt:
        pass
