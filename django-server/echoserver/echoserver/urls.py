from django.conf.urls import include, url
from django.contrib import admin
from hello import urls as echourl

urlpatterns = [
    # Examples:
    # url(r'^$', 'echoserver.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^echo/', include(echourl.urlpatterns)),
    url(r'^admin/', include(admin.site.urls)),
]
