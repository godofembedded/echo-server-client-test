from django.conf.urls import include, url
from . import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'echoserver.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^(?P<message>\w+)/$', views.myhello, name='echo_test'),
    # \w+ means
    # Word. Matches any ward character. + is Quantifier. Match 1 or more of the preceding token
]
